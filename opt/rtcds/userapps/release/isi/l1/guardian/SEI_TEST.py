import time
import subprocess

from guardian import GuardState, GuardStateDecorator
from guardian import NodeManager

import cdsutils
request = 'IDLE'
nominal = 'IDLE'


class INIT(GuardState):
    goto = True
    
    def main(self):
        return True

class IDLE(GuardState):
    goto = True
    index = 1

    def main(self):
        return True

class EARTHQUAKE_OFF(GuardState):
    goto = True
    index = 2
    def main(self):
        self.timer['wait'] = 60
        return False

    def run(self):
        if self.timer['wait']:
            return True
        return False

class EARTHQUAKE_ON(GuardState):
    goto = True
    index = 3
    def main(self):
        self.timer['wait'] = 60
        return False

    def run(self):
        if self.timer['wait']:
            return True
        return False

class PREP_EQ_MODE(GuardState):
    goto = True
    index = 4
    def main(self):
        self.timer['wait'] = 120
        return False

    def run(self):
        if self.timer['wait']:
            return True
        return False


edges = [
    ('INIT','IDLE'),
    ('IDLE','EARTHQUAKE_OFF'),
    ('IDLE','EARTHQUAKE_ON'),
    ('IDLE','PREP_EQ_MODE'),
    ('EARTHQUAKE_OFF','IDLE'),
    ('EARTHQUAKE_ON','IDLE'),
    ('PREP_EQ_MODE','IDLE')
]
